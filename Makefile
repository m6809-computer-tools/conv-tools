
CFLAGS = -DDEBUG

BINARIES = ddb2decb decb2ddb cas2decb image-patcher pad256

all: $(BINARIES)

clean:
	rm -f $(BINARIES)
