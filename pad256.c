/*
 * Copyright 2014 Tormod Volden
 *
 * Pads a file with zeroes until length is a multiple of 256
 *
 * Usage: pad256 file
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#ifndef O_BINARY
# define O_BINARY 0
#endif

int main (int argc, char **argv)
{
	int f;
	int olength;
	int ret;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s filename\n", argv[0]);
		exit(1);
	}

	f = open(argv[1], O_RDWR | O_APPEND | O_BINARY);
	if (f < 0) {
		fprintf(stderr, "open failed: %s\n", argv[1]);
		perror(NULL);
		exit(1);
	}
	olength = lseek(f, 0, SEEK_END) % 256;
	if (olength < 0) {
		perror("seek failed");
		exit(1);
	}
	if (olength > 0) {
		int i;

		for (i = olength; i < 256; i++) {
			ret = write(f, "\0", 1);
			if (ret != 1) {	
				perror("write failed");
				exit(1);
			}
		}
	}
	close(f);
	return 0;
}
