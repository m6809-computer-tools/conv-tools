/* 
 * (C) 2013 Tormod Volden
 *
 * Converts Dragon DOS binary file to DECB (RSDOS) file
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef WIN32
#include <fcntl.h>
#include <io.h>
#endif

/* Dragon DOS */
#define DDMAGIC 0x55
#define FILETYPE_BIN 0x02
#define DDMAGIC2 0xAA
/* DECB */
#define HEADMAGIC 0x00
#define TAILMAGIC 0xFF

int main (int argc, char **argv)
{
	unsigned char header[9];
	char *buffer;
	int length;
	int res;

#ifdef WIN32
	_setmode( _fileno( stdin ), _O_BINARY );
	_setmode( _fileno( stdout ), _O_BINARY );
#endif

	res = fread(header, 1, 9, stdin);
	if (res != 9) {
		fprintf(stderr, "Could not read Dragon DOS header\n");
		exit(1);
	}
	if (header[0] != DDMAGIC) {
		fprintf(stderr, "Wrong magic for Dragon DOS format\n");
		exit(1);
	}
	if (header[1] != FILETYPE_BIN) {
		fprintf(stderr, "Not supported file type value\n");
		exit(1);
	}
	if (header[8] != DDMAGIC2) {
		fprintf(stderr, "Wrong magic after header\n");
		exit(1);
	}


	printf("%c%c%c%c%c", HEADMAGIC,
               header[4], header[5],		/* Length */
	       header[2], header[3]);		/* Load address */

	length = (int) header[4] * 256 + (int) header[5];

	buffer = malloc(length);
	if (buffer == NULL) {
		fprintf(stderr, "Can not allocate memory\n");
		exit(1);
	}
	res = fread(buffer, 1, length, stdin);
	if (res != length) {
		fprintf(stderr, "Could not read %i bytes\n", length);
		exit(1);
	}
	res = fwrite(buffer, 1, length, stdout);
	free(buffer);
	if (res != length) {
		fprintf(stderr, "Could not write %i bytes\n", length);
		exit(1);
	}

	printf("%c%c%c%c%c",
	       TAILMAGIC, 0, 0,
	       header[6], header[7]);	/* EXEC address */

	exit(0);
}
