/*
 * Copyright 2013 Tormod Volden
 *
 * Patches a binary file using an ascii patch or DECB binary
 *
 * Usage: image-patcher image offset patch
 *
 * if patch starts with 0 byte -> DECB binary patch
 *
 * ascii patch format: address value value ... (all in hex)
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdio.h>
#include <stdlib.h>

FILE *open_image(char *fname)
{
	FILE *image;

	image = fopen(fname, "r+b");
	if (!image) perror(fname);
	return image;
}

FILE *open_patch(char *fname, int *image_type)
{
	FILE *patch;
	unsigned char first;

	patch = fopen(fname, "rb");
	if (!patch) perror(fname);
	fread(&first, 1, 1, patch);
	rewind(patch);
	if (first != 0x00) {
		/* reopen as text file, wherever that makes a difference */
		fclose(patch);
		patch = fopen(fname, "r");
		if (!patch) perror(fname);
	}
	*image_type = first;
	return patch;
}

int decb_patch(FILE *image, int offset, FILE *patch)
{
	int ret;
	unsigned char header[5];
	unsigned char *buffer;
	int length, address;
	int eof = 0;

	while (1) {
		ret = fread(header, 1, 5, patch);
		if (ret != 5) {
			fprintf(stderr, "Could not read section header from DECB file\n");
			return 1;
		}
		eof = header[0];
		length = header[1] * 256 + header[2];
		address = header[3] * 256 + header[4];
		if (eof) break;

		if (address < offset) {
			fprintf(stderr, "Patch address 0x%X is lower than offset 0x%X\n", address, offset);
			return 1;
		}

		buffer = malloc(length);
		if (!buffer) {
			fprintf(stderr, "Could not allocate buffer of size %i\n", length);
			return 1;
		}
		ret = fread(buffer, 1, length, patch);
		if (ret != length) {
			fprintf(stderr, "Could not read %i bytes from DECB file\n", length);
			return 1;
		}
		ret = fseek(image, address - offset, SEEK_SET);
		if (ret) {
			fprintf(stderr, "Could not seek to position %i\n", address);
			return 1;
		}
		ret = fwrite(buffer, 1, length, image);
		if (ret != length) {
			fprintf(stderr, "Could not write %i bytes into image file\n", length);
			return 1;
		}
	}
	return 0;
}

int ascii_patch(FILE *image, int offset, FILE *patch)
{
	int ret;
	int address;
	unsigned char value;
	char buffer[1024];
	int scanned, len;

	while (fgets(buffer, sizeof(buffer), patch)) {
		if (sscanf(buffer, "%x%n", &address, &scanned) != 1)
			continue;
		fprintf(stderr, "address %x\n", address);
		ret = fseek(image, address - offset, SEEK_SET);
		if (ret) {
			fprintf(stderr, "Could not seek to position %i\n", address);
			return 1;
		}
		while (sscanf(&buffer[scanned], "%2hhx%n", &value, &len) == 1) {
			scanned += len;
			fprintf(stderr, "value %02x\n", value);
			ret = fwrite(&value, 1, 1, image);
			if (ret != 1) {
				fprintf(stderr, "Could not write byte into image file\n");
				return 1;
			
			}
		}
	}
	return 0;
}

int main(int argc, char **argv)
{
	int ret = 1;
	FILE *image = NULL;
	FILE *patch = NULL;
	int offset;
	int patch_type;

	if (argc != 4) {
		fprintf(stderr, "Usage: %s image-file offset patch-file\n", argv[0]);
		exit(1);
	}

	offset = strtol(argv[2], NULL, 0);

	image = open_image(argv[1]);
	if (!image) goto done;

	patch = open_patch(argv[3], &patch_type);
	if (!patch) goto done;

	if (patch_type == 0)
		ret = decb_patch(image, offset, patch);
	else
		ret = ascii_patch(image, offset, patch);

done:
	if (image) fclose(image);
	if (patch) fclose(patch);
	return ret;
}
