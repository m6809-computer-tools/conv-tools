= Conversion tools =

From CAS file to DECB binary
cas2decb < file.cas > file.bin

From Dragon Data binary to DECB binary
ddb2decb < file1.bin > file2.bin

From DECB binary to Dragon Data binary
decb2ddb < file1.bin > file2.bin

== Patching of binary files ==

image-patcher image-file offset patch-file

The patch file is either a DECB binary or a text file in the format
(all hexadecimal):

address data data ...
address data ...
...

Examples:
echo "730A 4B 4A" > patch.txt &&
image-patcher prog.raw 0x7300 patch.txt
(will write 0x4B to the 10th byte, 0x4A to the 11th byte of prog.raw

image-patcher prog.raw 0x6000 patch.bin

