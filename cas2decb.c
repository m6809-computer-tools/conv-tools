/*
 * Copyright 2013-2014 Tormod Volden
 *
 * Converts CAS files to DECB binaries
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdio.h>
#include "portable.h"
#ifdef WIN32
#include <fcntl.h>
#include <io.h>
#endif

#ifdef DEBUG
#define debugp(...)	fprintf(stderr, __VA_ARGS__)
#else
#define debugp(...)
#endif

int checksum;
int inp_a = 0;
int file_len = 0;

unsigned char getb(void) {
	unsigned char inb;
	int ret;

	ret = fread(&inb, 1, 1, stdin);
	if (ret != 1)
		errx(1, "Could not read next byte after offset 0x%04X", inp_a);
	// debugp("Read %02X\n", inb);
	checksum += inb;
	inp_a++;
	return inb;
}

void putb(char outb) {
	int ret;

	ret = fwrite(&outb, 1, 1, stdout);
	if (ret != 1)
		errx(1, "Could not write byte");
	file_len++;
}

int main(int argc, char **argv)
{
	unsigned char inb;
	int btype = 0;
	int blen;
	char pname[8];
	int file_id, ascii_f, gap_f;
	int exec_a, load_a;
	int got_name_block = 0;
	int i;
	int out_a;

#ifdef WIN32
	_setmode( _fileno( stdin ), _O_BINARY );
	_setmode( _fileno( stdout ), _O_BINARY );
#endif

	while (btype != 0xFF) {
		while ((inb = getb()) == 0x55);
		/* while (inb != 0x3C) {
			warnx("Skipping %02X", inb);
			inb = getb();
		} */
		if (inb != 0x3C)
			errx(1, "Not sync byte: %02X at offset %04X", inb, inp_a);
		checksum = 0;
		btype = getb();
		blen = getb();
		// debugp("New block of type 0x%X and length %i\n", btype, blen);
		/* Namefile block */
		if (btype == 0x00) {
			for (i=0; i<8; i++)
				pname[i] = getb();
			file_id = getb();
			ascii_f = getb();
			gap_f = getb();
			if (file_id < 2)
				warnx("Not a M/L file");
			if (ascii_f == 0xFF)
				warnx("Ascii file");
			if (gap_f == 0xFF)
				warnx("Gapped file");
			exec_a = getb() * 256 + getb();
			load_a = getb() * 256 + getb();
			got_name_block = 1;
			if (blen != 15) {
				warnx("namefile block length %i", blen);
				for (i=15; i<blen; i++)
					getb();
			}
			if (file_id == 0)
				debugp("Program \"%8s\" BASIC at 0x%04X, execs at 0x%04X\n", pname, load_a, exec_a);
			else
				debugp("Program \"%8s\" loads at 0x%04X, execs at 0x%04X\n", pname, load_a, exec_a);
			out_a = load_a;
		/* EOF block */
		} else if (btype == 0xFF) {
			if (blen != 0) {
				warnx("EOF block has data length %i", blen);
				/* write decb data block */
				putb(0);	/* decb block type: data */
				putb(0);	/* length MSB */
				putb(blen);	/* length LSB */
				putb(out_a >> 8);
				putb(out_a & 0xFF);
				for (i=0; i<blen; i++)
					putb(getb());
			}
			putb(0xFF); 	/* decb block type: EOF */
			putb(0);
			putb(0);
			putb(exec_a >> 8);
			putb(exec_a & 0xFF);
		/* Data block */
		} else if (btype == 0x01) {
			if (!got_name_block)
				errx(1, "Data block before any namefile block");
			putb(0);	/* decb block type: data */
			putb(0);	/* length MSB */
			putb(blen);	/* length LSB */
			putb(out_a >> 8);
			putb(out_a & 0xFF);
			for (i=0; i<blen; i++)
				putb(getb());
			out_a += blen;
		} else {
			warnx("Unknown block type %i at offset 0x%04X", btype, inp_a - 2);
			for (i=0; i<blen; i++)
				getb();
		}
		/* Verify checksum of block */
		{
			int calc_check;

			calc_check = checksum & 0xFF;
			if (getb() != calc_check) {
				warnx("Checksum mismatch at offset 0x%04X, calculated 0x%02X", inp_a, calc_check);
			}
		}
	}
	/* pad file to be multiple of 256 (for DriveWire4) */
	while (file_len % 256)
		putb(0);
	return 0;
}

