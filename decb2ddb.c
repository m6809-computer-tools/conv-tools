/* 
 * (C) 2013 Tormod Volden
 *
 * Converts DECB (RSDOS) binary file to Dragon DOS file
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef WIN32
#include <fcntl.h>
#include <io.h>
#endif

/* Dragon DOS */
#define DDMAGIC 0x55
#define FILETYPE_BIN 0x02
#define DDMAGIC2 0xAA
/* DECB */
#define HEADMAGIC 0x00
#define TAILMAGIC 0xFF

int main (int argc, char **argv)
{
	unsigned char header[5];
	char *buffer;
	int length;
	int res;

#ifdef WIN32
	_setmode( _fileno( stdin ), _O_BINARY );
	_setmode( _fileno( stdout ), _O_BINARY );
#endif

	res = fread(header, 1, 5, stdin);
	if (res != 5) {
		fprintf(stderr, "Could not read DECB header\n");
		exit(1);
	}
	if (header[0] != HEADMAGIC ) {
		fprintf(stderr, "Wrong magic for DECB format\n");
		exit(1);
	}

	printf("%c%c%c%c%c%c", DDMAGIC, FILETYPE_BIN,
	       header[3], header[4],		/* Load address */
               header[1], header[2]);		/* Length */

	length = (int) header[1] * 256 + (int) header[2];

	buffer = malloc(length);
	if (buffer == NULL) {
		fprintf(stderr, "Can not allocate memory\n");
		exit(1);
	}
	res = fread(buffer, 1, length, stdin);
	if (res != length) {
		fprintf(stderr, "Could not read %i bytes\n", length);
		exit(1);
	}

	res = fread(header, 1, 5, stdin);	 /* Tail */
	if (res != 5) {
		fprintf(stderr, "Could not read DECB tail\n");
		exit(1);
	}
	if (header[0] != TAILMAGIC) {
		fprintf(stderr, "Wrong DECB tail magic\n");
		if (header[0] == 0x00)
			fprintf(stderr, "Segmented files not supported\n");
		exit(1);
	}
	if (header[1] != 0x00 || header[2] != 0x00) {
		fprintf(stderr, "Invalid DECB tail\n");
		exit(1);
	}

	printf("%c%c%c", header[3], header[4],	/* EXEC address */
	       DDMAGIC2);

	res = fwrite(buffer, 1, length, stdout);
	free(buffer);
	if (res != length) {
		fprintf(stderr, "Could not write %i bytes\n", length);
		exit(1);
	}
	exit(0);
}
